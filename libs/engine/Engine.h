//
// Created by dmitry on 24.10.2018.
//

#ifndef TEMPLATES_ENGINE_H
#define TEMPLATES_ENGINE_H

class Engine {
public:
    Engine();
    int run();
    ~Engine();
};

#endif //TEMPLATES_ENGINE_H
