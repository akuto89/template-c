//
// Created by dmitry on 24.10.2018.
//

#include "Engine.h"
#include <iostream>

Engine::Engine() {
    std::cout << "Engine" << std::endl;
}

int Engine::run() {
    std::cout << "Engine::run" << std::endl;
    return 5;
}

Engine::~Engine() {
    std::cout << "~Engine" << std::endl;
}