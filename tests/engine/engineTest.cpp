//
// Created by dmitry on 24.10.2018.
//

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "engine/Engine.h"

TEST_CASE("run1", "[1][full]") {
    Engine engine;
    REQUIRE(engine.run() == 5);
}

TEST_CASE("run2", "[2][full]") {
    Engine engine;
    REQUIRE(engine.run() < 6);
}