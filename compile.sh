#!/bin/sh

TESTS=ON

mkdir build
cd build

cmake -DBUILD_TESTING=${TESTS} ..
cmake --build . -- -j2

case ${TESTS} in
    ON) ctest -j2 -T test
    path=`cat ./Testing/TAG | head -1`
    cp Testing/$path/Test.xml test.xml
esac
